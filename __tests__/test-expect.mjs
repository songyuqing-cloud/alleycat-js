defineBinaryOperator ('|',  (...args) => pipe     (...args))
defineBinaryOperator ('<<', (...args) => compose    (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  ok, die, raise,
} from 'stick-js'

import {
  expectToEqual, expectNotToEqual,
  expectToBe, expectNotToBe,
  expectOk, expectNil,
  expectToMatchRegex, expectNotToMatchRegex,
  expectToBeInstanceOf, expectNotToBeInstanceOf,
  expectConstructorNameToBe, expectConstructorNameNotToBe,
  expectToBeTruthy, expectToBeFalsy,
  expectPredicate, expectNotPredicate,
  expectToHaveTypeof, expectNotToHaveTypeof,
  expectToHavePrimitiveType, expectNotToHavePrimitiveType,
  expectToContainObject, expectNotToContainObject,
  expectToRejectWithF, expectToRejectWith, expectToResolve, expectToResolveWith,
  expectToThrow, expectToThrowA, expectNotToThrow,
} from '../cjs/expect.js'

describe ('expectToEqual', () => {
  const x = {}
  const y = {}
  test ('+', () => {
    3 | expectToEqual (3)
    x | expectToEqual (y)
  })
  test ('-', () => {
    3 | expectNotToEqual (4)
  })
})

describe ('expectToBe', () => {
  const x = {}
  const y = {}
  test ('+', () => {
    x | expectToBe (x)
  })
  test ('-', () => {
    x | expectNotToBe (y)
  })
})

describe ('expectOk', () => {
  test ('+', () => {
    3 | expectOk
    false | expectOk
  })
  test ('-', () => {
    null | expectNil
    void 8 | expectNil
  })
})

describe ('expectToMatchRegex', () => {
  test ('+', () => {
    'thirty-five' | expectToMatchRegex (/^th\w+-five/)
    '' | expectToMatchRegex (/^$/)
  })
  test ('-', () => {
    'thirty-five' | expectNotToMatchRegex (/^thirty$/)
    '' | expectNotToMatchRegex (/./)
  })
})

describe ('expectToBeInstanceOf', () => {
  function Dog () {}
  function Cat () {}
  const dog = new Dog
  test ('+', () => {
    dog | expectToBeInstanceOf (Dog)
    new Error ('wut') | expectToBeInstanceOf (Error)
  })
  test ('-', () => {
    dog | expectNotToBeInstanceOf (Cat)
    new Error ('wut') | expectNotToBeInstanceOf (Dog)
  })
})

describe ('expectConstructorNameToBe', () => {
  function Dog () {}
  function Cat () {}
  const dog = new Dog
  test ('+', () => {
    dog | expectConstructorNameToBe ('Dog')
    new Error ('wut') | expectConstructorNameToBe ('Error')
  })
  test ('-', () => {
    dog | expectConstructorNameNotToBe ('Cat')
    new Error ('wut') | expectConstructorNameNotToBe ('Cat')
  })
})

describe ('expectToBeTruthy', () => {
  test ('+', () => {
    3 | expectToBeTruthy
    true | expectToBeTruthy
    '0' | expectToBeTruthy
  })
  test ('-', () => {
    0 | expectToBeFalsy
    false | expectToBeFalsy
    '' | expectToBeFalsy
    null | expectToBeFalsy
  })
})

describe ('expectPredicate', () => {
  const expectOk = ok | expectPredicate
  const expectNil = ok | expectNotPredicate
  test ('+', () => {
    3 | expectOk
    true | expectOk
    '0' | expectOk
  })
  test ('-', () => {
    null | expectNil
    void 8 | expectNil
  })
})

describe ('expectToHaveTypeof', () => {
  test ('+', () => {
    ; ({}) | expectToHaveTypeof ('object')
    ; [] | expectToHaveTypeof ('object')
    '' | expectToHaveTypeof ('string')
  })
  test ('-', () => {
    ; ({}) | expectNotToHaveTypeof ('string')
    ; [] | expectNotToHaveTypeof ('string')
    '' | expectNotToHaveTypeof ('object')
  })
})

describe ('expectToHavePrimitiveType', () => {
  test ('+', () => {
    ; ({}) | expectToHavePrimitiveType ('Object')
    ; [] | expectToHavePrimitiveType ('Array')
    '' | expectToHavePrimitiveType ('String')
  })
  test ('-', () => {
    ; ({}) | expectNotToHavePrimitiveType ('String')
    ; [] | expectNotToHavePrimitiveType ('String')
    '' | expectNotToHavePrimitiveType ('Object')
  })
})

describe ('expectToContainObject', () => {
  const a = { a: 1, b: 2, c: 3, }
  const b = { a: 1, b: 2, }
  const c = { c: 3, d: 4, }
  test ('+', () => {
    a | expectToContainObject (b)
    a | expectToContainObject ({})
  })
  test ('-', () => {
    a | expectNotToContainObject (c)
    a | expectNotToContainObject (['a', 'b'])
  })
})

describe ('expectToResolve/Reject', () => {
  const good = () => new Promise ((res, rej) => res ('good'))
  const bad = () => new Promise ((res, rej) => rej ('bad'))
  test ('+1', () => {
    return good | expectToResolve
  })
  test ('+2', () => {
    return good | expectToResolveWith ('good')
  })
  test ('-1', () => {
    return bad | expectToRejectWithF (
      (x) => x | expectToEqual ('bad'),
    )
  })
  test ('-2', () => {
    return bad | expectToRejectWith ('bad')
  })
})

describe ('expectToThrow', () => {
  function MyError () {}
  MyError.prototype = new Error
  const good = () => 10
  const bad1 = () => die ('bad')
  const bad2 = () => new MyError ('bad') | raise
  test ('+', () => {
    bad1 | expectToThrow
    bad2 | expectToThrowA (MyError)
  })
  test ('-', () => {
    good | expectNotToThrow
  })
})
