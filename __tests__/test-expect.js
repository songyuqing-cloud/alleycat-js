"use strict";

var _stickJs = require("stick-js");

var _expect = require("../cjs/expect.js");

describe('expectToEqual', function () {
  var x = {};
  var y = {};
  test('+', function () {
    (0, _stickJs.pipe)(3, (0, _expect.expectToEqual)(3));
    (0, _stickJs.pipe)(x, (0, _expect.expectToEqual)(y));
  });
  test('-', function () {
    (0, _stickJs.pipe)(3, (0, _expect.expectNotToEqual)(4));
  });
});
describe('expectToBe', function () {
  var x = {};
  var y = {};
  test('+', function () {
    (0, _stickJs.pipe)(x, (0, _expect.expectToBe)(x));
  });
  test('-', function () {
    (0, _stickJs.pipe)(x, (0, _expect.expectNotToBe)(y));
  });
});
describe('expectOk', function () {
  test('+', function () {
    (0, _stickJs.pipe)(3, _expect.expectOk);
    (0, _stickJs.pipe)(false, _expect.expectOk);
  });
  test('-', function () {
    (0, _stickJs.pipe)(null, _expect.expectNil);
    (0, _stickJs.pipe)(void 8, _expect.expectNil);
  });
});
describe('expectToMatchRegex', function () {
  test('+', function () {
    (0, _stickJs.pipe)('thirty-five', (0, _expect.expectToMatchRegex)(/^th\w+-five/));
    (0, _stickJs.pipe)('', (0, _expect.expectToMatchRegex)(/^$/));
  });
  test('-', function () {
    (0, _stickJs.pipe)('thirty-five', (0, _expect.expectNotToMatchRegex)(/^thirty$/));
    (0, _stickJs.pipe)('', (0, _expect.expectNotToMatchRegex)(/./));
  });
});
describe('expectToBeInstanceOf', function () {
  function Dog() {}

  function Cat() {}

  var dog = new Dog();
  test('+', function () {
    (0, _stickJs.pipe)(dog, (0, _expect.expectToBeInstanceOf)(Dog));
    (0, _stickJs.pipe)(new Error('wut'), (0, _expect.expectToBeInstanceOf)(Error));
  });
  test('-', function () {
    (0, _stickJs.pipe)(dog, (0, _expect.expectNotToBeInstanceOf)(Cat));
    (0, _stickJs.pipe)(new Error('wut'), (0, _expect.expectNotToBeInstanceOf)(Dog));
  });
});
describe('expectConstructorNameToBe', function () {
  function Dog() {}

  function Cat() {}

  var dog = new Dog();
  test('+', function () {
    (0, _stickJs.pipe)(dog, (0, _expect.expectConstructorNameToBe)('Dog'));
    (0, _stickJs.pipe)(new Error('wut'), (0, _expect.expectConstructorNameToBe)('Error'));
  });
  test('-', function () {
    (0, _stickJs.pipe)(dog, (0, _expect.expectConstructorNameNotToBe)('Cat'));
    (0, _stickJs.pipe)(new Error('wut'), (0, _expect.expectConstructorNameNotToBe)('Cat'));
  });
});
describe('expectToBeTruthy', function () {
  test('+', function () {
    (0, _stickJs.pipe)(3, _expect.expectToBeTruthy);
    (0, _stickJs.pipe)(true, _expect.expectToBeTruthy);
    (0, _stickJs.pipe)('0', _expect.expectToBeTruthy);
  });
  test('-', function () {
    (0, _stickJs.pipe)(0, _expect.expectToBeFalsy);
    (0, _stickJs.pipe)(false, _expect.expectToBeFalsy);
    (0, _stickJs.pipe)('', _expect.expectToBeFalsy);
    (0, _stickJs.pipe)(null, _expect.expectToBeFalsy);
  });
});
describe('expectPredicate', function () {
  var expectOk = (0, _stickJs.pipe)(_stickJs.ok, _expect.expectPredicate);
  var expectNil = (0, _stickJs.pipe)(_stickJs.ok, _expect.expectNotPredicate);
  test('+', function () {
    (0, _stickJs.pipe)(3, expectOk);
    (0, _stickJs.pipe)(true, expectOk);
    (0, _stickJs.pipe)('0', expectOk);
  });
  test('-', function () {
    (0, _stickJs.pipe)(null, expectNil);
    (0, _stickJs.pipe)(void 8, expectNil);
  });
});
describe('expectToHaveTypeof', function () {
  test('+', function () {
    ;
    (0, _stickJs.pipe)({}, (0, _expect.expectToHaveTypeof)('object'));
    (0, _stickJs.pipe)([], (0, _expect.expectToHaveTypeof)('object'));
    (0, _stickJs.pipe)('', (0, _expect.expectToHaveTypeof)('string'));
  });
  test('-', function () {
    ;
    (0, _stickJs.pipe)({}, (0, _expect.expectNotToHaveTypeof)('string'));
    (0, _stickJs.pipe)([], (0, _expect.expectNotToHaveTypeof)('string'));
    (0, _stickJs.pipe)('', (0, _expect.expectNotToHaveTypeof)('object'));
  });
});
describe('expectToHavePrimitiveType', function () {
  test('+', function () {
    ;
    (0, _stickJs.pipe)({}, (0, _expect.expectToHavePrimitiveType)('Object'));
    (0, _stickJs.pipe)([], (0, _expect.expectToHavePrimitiveType)('Array'));
    (0, _stickJs.pipe)('', (0, _expect.expectToHavePrimitiveType)('String'));
  });
  test('-', function () {
    ;
    (0, _stickJs.pipe)({}, (0, _expect.expectNotToHavePrimitiveType)('String'));
    (0, _stickJs.pipe)([], (0, _expect.expectNotToHavePrimitiveType)('String'));
    (0, _stickJs.pipe)('', (0, _expect.expectNotToHavePrimitiveType)('Object'));
  });
});
describe('expectToContainObject', function () {
  var a = {
    a: 1,
    b: 2,
    c: 3
  };
  var b = {
    a: 1,
    b: 2
  };
  var c = {
    c: 3,
    d: 4
  };
  test('+', function () {
    (0, _stickJs.pipe)(a, (0, _expect.expectToContainObject)(b));
    (0, _stickJs.pipe)(a, (0, _expect.expectToContainObject)({}));
  });
  test('-', function () {
    (0, _stickJs.pipe)(a, (0, _expect.expectNotToContainObject)(c));
    (0, _stickJs.pipe)(a, (0, _expect.expectNotToContainObject)(['a', 'b']));
  });
});
describe('expectToResolve/Reject', function () {
  var good = function good() {
    return new Promise(function (res, rej) {
      return res('good');
    });
  };

  var bad = function bad() {
    return new Promise(function (res, rej) {
      return rej('bad');
    });
  };

  test('+1', function () {
    return (0, _stickJs.pipe)(good, _expect.expectToResolve);
  });
  test('+2', function () {
    return (0, _stickJs.pipe)(good, (0, _expect.expectToResolveWith)('good'));
  });
  test('-1', function () {
    return (0, _stickJs.pipe)(bad, (0, _expect.expectToRejectWithF)(function (x) {
      return (0, _stickJs.pipe)(x, (0, _expect.expectToEqual)('bad'));
    }));
  });
  test('-2', function () {
    return (0, _stickJs.pipe)(bad, (0, _expect.expectToRejectWith)('bad'));
  });
});
describe('expectToThrow', function () {
  function MyError() {}

  MyError.prototype = new Error();

  var good = function good() {
    return 10;
  };

  var bad1 = function bad1() {
    return (0, _stickJs.die)('bad');
  };

  var bad2 = function bad2() {
    return (0, _stickJs.pipe)(new MyError('bad'), _stickJs.raise);
  };

  test('+', function () {
    (0, _stickJs.pipe)(bad1, _expect.expectToThrow);
    (0, _stickJs.pipe)(bad2, (0, _expect.expectToThrowA)(MyError));
  });
  test('-', function () {
    (0, _stickJs.pipe)(good, _expect.expectNotToThrow);
  });
});

