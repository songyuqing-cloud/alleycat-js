## v0.3.10

*2021-07-11*

- Internal changes.

## v0.3.8

*2021-07-11*

- 'react-intl' was moved to peer dependencies.

## v0.3.5

*2021-07-10*

- Added more /es aliases in package.json.

## v0.3.3

*2021-07-09*

- Updated reselect import.

## v0.3.2

*2021-07-09*

- Added `recoverAndBounce`.
- Added warnings to deprecated async functions in alleycat-js/general.

## v0.1.14

*2020-07-13*

- alleycat-js/general:

    - `defaultToV` is now curried.

- alleycat-js/configure:

    - Rewritten with new API. See examples.

## v0.1.13

*2020-04-26*

- alleycat-js/select:

    - `select` takes a different argument order now: the selectorName for debugging comes first.

## v0.1.12

*2020-04-12*

- alleycat-js/redux:

    - added `container`

    - the values of `selectorTable` as given to `container*` must now be values, not functions;
      i.e., selectors, not selector factories in our normal usage.

- added alleycat-js/select.

- removed alleycat-js/reselect.

## v0.1.11

*2020-04-10*

- Add `invariant` to dependencies.

## v0.1.10

*2020-04-09*

- alleycat-js/redux:

    - the `reducer` function has been overhauled (see examples), and `traceReducer` is no longer
      exported.

## v0.1.9

*2020-04-08*

- alleycat-js/react:

    - `transformIntl` and `transformIntlComponent` are no longer exported.

## v0.1.8

*2020-04-08*

- alleycat-js/general:

    - removed `remapTuples` and `remapTuplesIn`.

- alleycat-js/react:

    - updated `prepareIntl` to use `remapTuples` from stick.

## v0.1.7

*2020-04-08*

- alleycat-js/general:

    - moved `transformIntl`, `transformIntlComponent` to `react`.

- alleycat-js/react:

    - added `prepareIntl`, `prepareIntlComponent`.

## v0.1.6

*2020-04-07*

- alleycat-js/react:

    - add `loadable`.

## v0.1.5

*2020-04-07*

- alleycat-js/react:

    - `whyWhen` is now called `useWhyTell`.
    - `shouldUpdateWith`, `shouldUpdate`, `memoWithIgnoreTell`, `memoIgnoreTell` all take a logger instead of a tag now.
    - new functions: `withMemoTell`, `withMemo`, `withEffect`, `componentTell`, `component`, `containerTell`, and `container`.

## v0.1.4

*2020-04-05*

- alleycat-js/general:

    - Add `conformsTo`

- alleycat-js/redux:

    - Add `checkStore`

- alleycat-js/reduxHooks.js:

    - New. Contains `useReduxReducer` and `useSaga`.

## v0.1.3

*2020-03-31*

- alleycat-js/react:

    - Add `whyWhen`

    - Update examples.

## v0.1.2

*2020-03-30*

- Update render message in `whyYouRerender`.

## v0.1.1

*2020-03-30*

- Update license to GPL-3.0

## v0.1.0

*2020-03-30*

- Legacy (class-based) functions have been moved from `alleycat-js/react` to
  `alleycat-js/react-legacy`.

- Changes to `useMeasureWithAfter`:

    - It is now called `useMeasureWithCb`.

    - The callback function is now called with the arguments `node` and `measure`. `measure` can be
      used to repeat the measurement. The two measurement arguments have been dropped.

## v0.0.65

- Beginning of changelog.
