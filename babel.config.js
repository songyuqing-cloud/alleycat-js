const config = (modules) => ({
  presets: [
    [
      '@babel/env',
      {
        modules,
        targets: {
          chrome: 58,
          ie: 11,
        },
      },
    ],
  ],
  plugins: [
    // --- reduce code size and avoid namespace pollution (e.g. global
    // polyfills; be sure to add @babel/runtime to runtime deps)
    '@babel/transform-runtime',
    'alleycat-stick-transforms',
  ],
})

module.exports = (api) => {
  api.cache.forever ()
  return {
    // --- this comes from babel itself and is separate from @babel/env.
    env: {
      // --- keyed on BABEL_ENV
      es: config (false),
      cjs: config ('commonjs'),
    },
  }
}
