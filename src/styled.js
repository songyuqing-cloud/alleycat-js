; `
Usage:

Put something like this in a common location and design your breakpoints as you wish:

export const mediaPhone      = 0    | mgt | mediaRule
export const mediaPhoneBig   = 576  | mgt | mediaRule
export const mediaTablet     = 768  | mgt | mediaRule
export const mediaDesktop    = 992  | mgt | mediaRule
export const mediaDesktopBig = 1200 | mgt | mediaRule

Then in the component:

  \${mediaQuery (
    mediaPhone (\`
      color: yellow;
      font-size: 20px;
    \`),
    mediaTablet (\`
      color: green;
      font-size: 15px;
    \`),
  )}

If you need the props:

  \${props => mediaQuery (
    mediaPhone (\`
      color: yellow;
      font-size: 20px;
    \`),
    mediaTablet (\`
      color: green;
      font-size: 15px;
    \`),
  )}
`

import {
  pipe, compose, composeRight,
  sprintfN, join, map,
} from 'stick-js'

;`
  mediaRule := mediaClause AND widthClauses { css }
  mediaClause := '@media only screen'
  widthClauses := widthClause (AND widthClause ...)
  widthClause := (min-width: n) | (max-width: n)
`

// --- [comp, val] -> String
const widthRule = sprintfN ('(%s: %spx)')
const widthClauses = map (widthRule) >> join (' and ')

const mediaCondition = (spec) => [
  '@media all',
  spec | widthClauses,
] | join (' and ')

export const mlt = x => [['max-width', x]]
export const mgt = x => [['min-width', x]]
export const mwithin = ([x, y]) => [['min-width', x], ['max-width', y]]

export const mediaRule = (spec) => (css) => [mediaCondition (spec), css] | sprintfN ('%s {%s}')
export const mediaQuery = (...lines) => lines | join ('\n')

// --- @deprecated
export const mediaComp = (...css) => (spec) => mediaRule (spec) (css | join ('\n'))
export const media = (...lines) => lines | join ('\n')
