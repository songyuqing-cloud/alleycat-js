// --- bilby.js has no alleycat-js dependencies.


import {
  pipe, composeRight, compose,
  dot, prop,
  dot2, dot1, dot3,
  ifOk, always, id, noop,
  head, tail,
  bindPropTo,
  die, concatTo,
  not,
  recurry,
} from 'stick-js/es'

import bilby from 'bilby'
const { left: Left, right: Right, some: Just, none: Nothing, } = bilby

export {
  Left, Right,
  Just, Nothing,
}

export const isLeft = prop ('isLeft')
export const fold = dot2 ('fold')
export const fold3 = dot3 ('fold')
export const flatMap = dot1 ('flatMap')

export const toEither = l => ifOk (
  Right,
  l | Left | always,
)

export const toMaybe = ifOk (Just, Nothing | always)

export const isJust = prop ('isSome')
export const isNothing = isJust >> not

export const foldJust = f => fold (
  f, void 8,
)

export const toJust = fold (id, void 8)

const colon = (x, xs) => [x, ...xs]
const bilbyLiftA2 = 'liftA2' | bindPropTo (bilby)

// --- note that `f` must be curried (manual is ok).
export const liftA2 = recurry (3) (
  f => x => y => bilbyLiftA2 (f, x, y)
)

// --- note that `f` must be curried (manual is ok).
export const liftA2N = recurry (2) (
  f => xs => bilbyLiftA2 (f, ...xs)
)

// @test
export const sequenceM = (pure) => {
  const _sequence = xs => xs.length === 0
    ? ([] | pure)
    : bilbyLiftA2 (colon, xs | head, xs | tail | _sequence)
  return _sequence
}

export const cata = dot1 ('cata')

export const foldRight = decorate => fold (
  l => l | concatTo (decorate + ' ') | die,
  id,
)
