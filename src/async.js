import {
  pipe, composeRight, compose,
  dot, prop, dot1,
  recurry,
  tap, always,
} from 'stick-js/es'

import { Left, Right, } from './bilby.js'

const V = void 8 | always

export const then = dot1 ('then')
export const recover = dot1 ('catch')

export const thenTap = recurry (2) (
  f => p => p.then ((...args) => (f (...args), p))
)

/*
 * Usage: at the end of a promise chain:
 *
 * e.g.
 *
 *  fetch (url, options)
 *  | then/recover (...)
 *  | then/recover (...)
 *  | ...
 *  | promiseToEither
 *
 * e.g. in a saga:
 *
 *  const results = yield fetch (url, options)
 *    | then/recover (...)
 *    | ...
 *    | promiseToEither
 *
 * // --- results is an Either and execution is sychronous again.
 */

export const promiseToEither = then (Right) >> recover (Left)

export const resolveP = (val) => Promise.resolve (val)
export const rejectP  = (val) => Promise.reject (val)
export const allP     = (ps)  => Promise.all (ps)
export const startP   = ()    => null | resolveP

/* Catch a promise rejection, run `f`, and then re-reject it.
 * Useful for e.g. error messages.
 * Note that we must be sure to return undefined, so that the promise doesn't automatically resolve.
 */
export const recoverAndBounce = (f) => recover (tap (f) >> rejectP >> V)
