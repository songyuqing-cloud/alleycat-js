import {
  pipe, composeRight, compose,
  dot,
} from 'stick-js/es'

export const dag = type => x => type.is (x)
export const toJS = dot ('toJS')
