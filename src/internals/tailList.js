import {
  pipe, compose, composeRight,
  tap, appendM, id,
  factory, factoryProps,
} from 'stick-js/es'

const proto = {
  init () {
    this.vals = []
    return this
  },
  push (x) {
    const f = ((this.m += 1) <= this.n) ? id : appendM (x)
    return this.vals | f
  },
  get () {
    return this.vals
  },
}

const props = {
  n: void 8,
  m: 0,
  vals: void 8,
}

export default proto | factory | factoryProps (props)
