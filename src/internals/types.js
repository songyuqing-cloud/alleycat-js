import {
  pipe, compose, composeRight,
  ok, ifOk, ifPredicate, whenOk, whenPredicate,
  id, tap, recurry,
  map, filter, reject, reduce, flip, flip3,
  join, split, last, head, tail,
  dot, dot1, dot2, side, side1, side2,
  cond, condS, guard, guardV, otherwise,
  sprintf1, sprintfN,
  noop, blush, always, T, F,
  prop, path,
  lets, letS,
  die, eq, ne,
  factory, factoryProps,
} from 'stick-js/es'

import daggy from 'daggy'

import { Just, cata, fold, fold3, } from '../bilby.js'
import { logWith, } from '../general.js'

// --- internal to this module.
const Result = daggy.taggedSum ('Result', {
  ResultOk: ['result'],
  // --- we could consider adding imsgMb to this case as well, but for now we don't to keep it
  // simple and because the umsg is probably enough.
  ResultUserError: ['umsg'],
  ResultNonUserError: ['imsgMb'],
})

const { ResultOk, ResultUserError, ResultNonUserError, } = Result

Result.prototype.fold = function (ok, user, nonUser) {
  return this | cata ({
    ResultOk: result => result | ok,
    ResultUserError: umsg => umsg | user,
    ResultNonUserError: imsgMb => imsgMb | nonUser,
  })
}

/* Hard to think of a good name.
 * Transforms a ResultOk into either ResultOk or ResultNonUserError, and keeps ResultUserError and
 * ResultNonUserError the same.
 * Takes a function `f`, which takes the result of the ResultOk case, and returns an Either. If the
 * Either resolves to left, return ResultNonUserError; if right, ResultOk.
 * See the various sagas for how this could be useful, e.g., the request was successful, but now you
 * want to transform the results in a way which can fail, or it returned an 'err' field despite
 * being successful.
 */

Result.prototype.resultFoldMap = function (f) {
  return this | cata ({
    ResultOk: result => result | f | fold (
      (left) => left | Just | ResultNonUserError,
      (right) => right | ResultOk,
    ),
    ResultUserError: ResultUserError,
    ResultNonUserError: ResultNonUserError,
  })
}

Result.prototype.map = function (f) {
  return this | cata ({
    ResultOk: result => result | f | ResultOk,
    ResultUserError: ResultUserError,
    ResultNonUserError: ResultNonUserError,
  })
}

export const resultFoldMap = dot1 ('resultFoldMap')
export const resultFold = fold3

export {
  ResultOk, ResultUserError, ResultNonUserError,
}
