import {
  pipe, compose, composeRight,
  lets, sprintf1,
  againstAll, isString, isFunction,
} from 'stick-js/es'

import React from 'react'
const { useEffect, useContext, } = React

// --- @peer
import { ReactReduxContext, } from 'react-redux'

import invariant from 'invariant'

import { isNotEmptyString, allV, } from '../predicate.js'

import { checkStore, } from '../redux.js'

const isStringAndNotEmpty = againstAll ([
  isString, isNotEmptyString,
])

const injectReducerFactory = (store, isValid) => {
  return (key, reducer, createReducer) => {
    if (!isValid) checkStore (store)

    invariant (
      allV (
        key | isStringAndNotEmpty,
        reducer | isFunction,
      ),
      'injectReducerFactory: Invalid arguments',
    )

    // --- react-boilerplate:
    // check `store.injectedReducers[key] === reducer` for hot reloading when a key is the same but
    // a reducer is different
    if (Reflect.has (store.injectedReducers, key) && store.injectedReducers[key] === reducer) return

    store.injectedReducers [key] = reducer
    store.replaceReducer (createReducer (store.injectedReducers))
  }
}

const getInjectors = (store) => {
  // --- @todo why do we need to check the store again here?
  checkStore (store)

  return {
    injectReducer: injectReducerFactory (store, true),
  }
}

export const useReduxReducer = ({ key, reducer, createReducer, }) => {
  const store = useContext (ReactReduxContext).store
  useEffect (() => {
    getInjectors (store).injectReducer (key, reducer, createReducer)
  }, [])
}
