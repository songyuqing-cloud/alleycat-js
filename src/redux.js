import {
  pipe, compose, composeRight,
  always, id,
  tap, condS, guard, otherwise, ne,
  noop,
  sprintfN,
  isArray, ifPredicate,
  invoke, recurry,
  isFunction, isObject,
} from 'stick-js/es'

import invariant from 'invariant'

import { logWith, composeManyRight, conformsTo, } from './general.js'

const ifArray = isArray | ifPredicate

/*
 * Usage:
 *
 *   const reducerTable = {
 *     // --- constant to listen for (i.e. `type` of the action payload).
 *     [constant1]:
 *       (actionPayload) => (curState) =>
 *         // --- the new state -- be sure it's an immutable update.
 *         <new-state>,
 *
 *     // --- the updater can also be a list of immutable update operations, in which case they'll
 *     be composed in order:
 *
 *     [constant2]: (actionPayload) => [update1, update2, ...],
 *
 *     ...
 *   }
 *
 * Example:
 *
 *   import { update, merge, plus, } from 'stick-js'
 *
 *   const initialState = {
 *     curTime: 0,
 *     ...
 *   }
 *
 *   const reducerTable = {
 *     [TIMER_ADVANCE]: ({ data: millis, }) => (curState) => ({
 *       ...curState,
 *       curTime: curState.curTime + millis,
 *     }),
 *
 *     // --- or
 *     [TIMER_ADVANCE]: ({ data: millis, }) => update (
 *       'curTime',
 *       plus (millis),
 *     }),
 *
 *     [TIMER_SET]: ({ data: millis, }) => (curState) => ({
 *       ...curState,
 *       curTime: millis,
 *     }),
 *
 *     // --- or
 *     [TIMER_SET]: ({ data: millis, }) => merge ({
 *       curTime: millis,
 *     }),
 *   }
 *
 */

export const checkStore = invoke (() => {
  const shape = {
    dispatch: isFunction,
    subscribe: isFunction,
    getState: isFunction,
    replaceReducer: isFunction,
    runSaga: isFunction,
    injectedReducers: isObject,
    injectedSagas: isObject,
  }

  return (store) => invariant (
    store | conformsTo (shape),
    '(app/utils...) injectors: Expected a valid redux store',
  )
})

export const reducerTell = invoke (() => {
  const _reducer = reducerTable => action => {
    const updater = action | (reducerTable [action.type] || always (id))
    return updater | ifArray (
      (us) => composeManyRight (...us),
      id,
    )
  }

  const traceReducer = (reducerName, tellSpec, action) => tap (
    (state) => {
      if (!tellSpec) return
      if (tellSpec !== true && !tellSpec [reducerName])
        return
      return state | logWith ([reducerName, action | JSON.stringify] |
        sprintfN ('[reducer %s]\n→ fired action = %s\n→ new state ='),
      )
    }
  )

  return recurry (4) (
    (tellSpec) => (name) => (initialState) => (reducerTable) =>
      (state=initialState, action) => state
        | _reducer (reducerTable) (action)
        | traceReducer (name, tellSpec, action),
  )
})

export const reducer = null | reducerTell
