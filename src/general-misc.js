import {
  pipe, composeRight, compose,
  prop, map, addIndex,
  always, id,
  not, T, F, join,
  ok, dot1, dot2, side2,
  guard, otherwise,
  lets,
  concat, condS, guardV,
  againstEither,
  timesV,
  tryCatch,
  path,
  split,
  isType,
  ifPredicate,
  head,
  recurry,
} from 'stick-js/es'

// --- predicate has no alleycat-js dependencies.
import { ifLongerThan, ifNegativeOne, ifType, } from './predicate.js'
// --- bilby has no alleycat-js dependencies.
import { isJust, toJust, Nothing, Just, Left, Right, } from './bilby.js'

const removeSpaces = dot2 ('replace') (/\s+/g) ('')

// --- beware, overwrites any flags that the re already had.
export const xRegExpFlags = (re, flags) => new RegExp (
  re.source | removeSpaces,
  flags,
)

export const findPredOk = recurry (2) (
  (pred) => (xs) => {
    for (const x of xs) {
      const p = pred (x)
      if (ok (p)) return p
    }
  }
)

export const findPredOkGen = recurry (2) (
  (pred) => (gen) => {
    let n
    while (! (n = gen.next ()).done) {
      const p = pred (n.value)
      if (ok (p)) return p
    }
  }
)

export const findPredMaybeGen = recurry (2) (
  (pred) => (gen) => {
    let n
    while (! (n = gen.next ()).done) {
      const p = pred (n.value)
      if (p | isJust) return p | toJust
    }
  }
)

export const tryCatchS = recurry (4) (
  good => bad => f => v => tryCatch (good, bad, _ => f (v))
)

// ------ lazyfish extensions
export const lazyFindPred = recurry (2) ((pred, lxs) => {
  while (true) {
    const { value, done, } = lxs.next ()
    if (done) break
    const predVal = pred (value)
    if (predVal) return predVal
  }
})

export const substring = dot2 ('substring')

export const ellipsisAfter = recurry (2) (
  n => s => s | ifLongerThan (n) (
    substring (0, n) >> concat ('…'),
    id,
  )
)

// --- doesn't truncate if too long.
export const padTo = recurry (2) (
  n => str => lets (
    _ => str.length,
    (l) => str | condS ([
      (_ => l >= n) | guardV (str),
      otherwise | guard (x => x | (timesV (n - l) (' ') | join ('') | concat)),
    ])
  )
)

export const on = side2 ('on')

export const uniqueWith = (f) => (xs) => {
  const ret = []
  const s = new Set
  for (const x of xs) {
    const xx = f (x)
    if (s.has (xx)) continue
    ret.push (x)
    s.add (f (x))
  }
  return ret
}

export const errString = prop ('message')
export const errFull = ifType ('Error') (
  prop ('stack'),
  id,
)
