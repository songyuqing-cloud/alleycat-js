// @todo change name of module


import {
  pipe, compose, composeRight,
  id, tap, ok, whenOk, dot, dot1, notOk, isTrue,
  guard, otherwise, condS, guardV,
  die,
} from 'stick-js/es'

import AlertMod from 'react-s-alert'
import 'react-s-alert/dist/s-alert-default.css'
import '../css/react-s-alert-slide.css'

import {
  ierror, error as err
} from './general.js'

const Alert = AlertMod.default

const msg = condS ([
  notOk     | guardV (''),
  isTrue    | guardV ('Oops, something went wrong!'),
  otherwise | guard (id),
])

const _error = opts => txt => Alert.error (txt, opts)

export const warning = msg >> Alert.warning
export const error = msg >> _error ({})
export const info = msg >> Alert.info
export const success = msg >> Alert.success
export const close = id => Alert.close (id)
export const closeAll = Alert.closeAll

// @todo need a better name for this.
// it's for showing a specific programmer error in the console and a generic 'oops' in the bubble.
export const ierrorError = ierror >> tap (_ => true | error)
// ditto, but dies afterwards.
export const ierrorErrorDie = ierrorError >> die

export const errorError = err >> tap (_ => true | error)
export const errorErrorDie = errorError >> die

export const error1 = opts => msg >> _error (opts)
