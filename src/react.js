import {
  pipe, compose, composeRight,
  recurry, isObject, join, sprintfN,
  map, appendToM, condS, guard,
  isString, isArray, isFunction, otherwise,
  tap, whenOk, noop, sprintf1,
  not, merge, mergeTo, lets,
  arg0, arg2, deconstruct2,
  id, invoke, ifOk, concatTo,
  mapTuples, mergeM,
} from 'stick-js/es'

// --- @peer
import React from 'react'
import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

const { Suspense, createElement, lazy, memo, useRef, useEffect, useCallback, useState, PureComponent, } = React

import { truncate, debug, } from './general.js'
import { whenEquals, ifArray, } from './predicate.js'

// ------ general.

export const Elem = elem => React.createElement (elem)
export const ElemP = recurry (2) (
  elem => withDisplayName ('<ElemP>') (
    props => React.createElement (elem, props),
  ),
)
export const ElemPostProps = recurry (3) (
  propsPost => elem => props => lets (
    () => props | merge (propsPost),
    ElemP (elem),
  )
)
export const ElemPreProps = recurry (3) (
  propsPre => elem => props => lets (
    () => props | mergeTo (propsPre),
    ElemP (elem),
  )
)

/*
 * Wraps `deconstruct2`, meaning the props are passed twice, once for deconstructing and once for
 * passing whole. This is useful if you're wrapping several components and want to pass individual
 * props in some cases, and all of them at once, including children, in others.
 *
 * Note that components receive props and context as arguments, so we use arg0 to get only the
 * props.
 *
 * Example:
 *
 *   const SomeComponent = deconstructProps (
 *     ({ prop1, prop2, ... }) => props => <Inner1
 *       prop1={prop1}
 *       prop2={prop2}
 *     >
 *       <Inner2 {...props}/>
 *     </Inner1>
 *   )
 */
export const deconstructProps = f => arg0 >> deconstruct2 (f)

export const withDisplayName = recurry (2) (
  displayName => mergeM ({ displayName, }),
)

/*
 * Example (using old-style classes):
 * See examples by `whyRender` for hook-style.
 *
 *   componentDidUpdate (prevProps, prevState) {
 *     if (debugRender) whyYouRerender ('Header', [prevProps, this.props])
 *     // --- or
 *     if (debugRender) whyYouRerender ('Header', [prevProps, this.props], [prevState, this.state])
 *   }
 */

export const whyYouRerender = invoke (() => {
  const stringify = JSON.stringify >> whenOk (truncate (30))
  const makeString = condS ([
    isObject   | guard (stringify >> concatTo ('[object] ')),
    isArray    | guard (stringify >> concatTo ('[array] ')),
    isFunction | guard (() => '[function]'),
    isString   | guard (stringify),
    otherwise  | guard (stringify),
  ])

  const record = (changed, what, which, cur, nxt, ignore) => lets (
    () => [cur, nxt] | map (makeString),
    () => ignore ? '[ignore] ' : '',
    ([curStr, nxtStr], ignoreStr) => [what, ignoreStr, which, curStr, nxtStr] |
      sprintfN ('  [%s] %s%s: %s -> %s'),
    arg2 >> appendToM (changed),
  )
  return (msgTag, [curProps, nxtProps, ignoreProps=[]], [curState, nxtState, ignoreState=[]]=[]) => {
    const changed = []
    const ignorePropsS = new Set (ignoreProps)
    const ignoreStateS = new Set (ignoreState)
    let cur, nxt
    const spec = [['prop', curProps, nxtProps, ignorePropsS]]
    if (curState) spec.push (['state', curState, nxtState, ignoreStateS])
    for (const [what, curs, nxts, ignore] of spec)
      for (const k in nxts)
        if ((nxt = nxts [k]) !== (cur = curs [k])) lets (
          () => ignore.has (k),
          (ignore) => record (changed, what, k, cur, nxt, ignore),
        )
    const header = msgTag | sprintf1 ('[component %s] render')
    const propStateStr = 'props (and/or state) you provided'
    if (changed.length) debug ([header, changed | join ('\n')] | sprintfN ('%s:\n%s'))
    else debug ([header, propStateStr] | sprintfN (
      `%s, although %s seem to be the same. This is usually due to the parent component updating ` +
      `(memoising this component may prevent this), ` +
      `props or state you forgot to provide to \`whyYouRerender\`, hidden props like \`key\`, ` +
      `or \`forceUpdate()\` in legacy code`,
    ))
  }
})

/*
 * Examples:
 *
 *   in config:
 *     const debugRender = true
 *     // or
 *     const debugRender = {
 *       Lemma: true,
 *       ...
 *     }
 *     // or
 *     const debugRender = null
 *
 *   const why = useWhyTell (debugRender)
 *   const Lemma = (props) => {
 *     why ('Lemma', props)
 *     why ('Lemma', props, { background, border, })
 *     why ('Lemma', [props, ignoreProps], { background, border, })
 *     // --- this case is pretty rare (you could just remove the ignored state from the state
 *     object), but it could be useful maybe.
 *     why ('Lemma', [props, ['onClick'], [{ background, border, }, ['background'])
 *   }
 */

export const whyRender = (
  tag,
  propsSpec,
  stateSpec=[{}, []],
) => {
  // --- only do this effect on updates, not mount.
  const mounted = useRef (false)
  const [props, ignoreProps] = propsSpec | ifArray (
    id,
    () => [propsSpec, []],
  )
  const [state, ignoreState] = stateSpec | ifArray (
    id,
    () => [stateSpec, []],
  )
  const prevProps = usePrevious (props) || {}
  const prevState = usePrevious (state) || {}
  useEffect (
    () => {mounted.current && whyYouRerender (
      tag, [prevProps, props, ignoreProps], [prevState, state, ignoreState],
    )},
  )
  mounted.current = true
}

/* This function needs to be fast (it is called on every render). Return immediately if not
 * debugging.
 *
 * `tellSpec`:
 *   Any falsey value: don't debug this render.
 *   `true`: debug this render.
 *   A table: debug this render if the component name exists in the table with a truthy value.
 *
 * Examples/usage: see `whyRender`
 */

export const useWhyTell = (tellSpec) => (componentName, ...rest) => {
  if (!tellSpec) return
  if (tellSpec !== true && !tellSpec [componentName])
    return
  return whyRender (componentName, ...rest)
}

export const useDidMount = f => useEffect (f, [])

export const useDidUpdate = (f, xs=null) => lets (
  () => useRef (false),
  (mounted) => useEffect (() => {
    if (mounted.current) f ()
    mounted.current = true
  }, xs),
)

export const usePrevious = (cur) => {
  const prev = useRef (null)
  useEffect (() => {
    prev.current = cur
  })
  return prev.current
}

export const cmpEq = (ignoreProps) => {
  const skipKeysPropsSet = new Set (ignoreProps)
  return (props, nextProps) => {
    for (const p in nextProps)
      if (!skipKeysPropsSet.has (p) && nextProps [p] !== props [p])
        return p
    return null
  }
}

/*
 * Returns a function which, given `props` and `nextProps`, returns `true` if they are the same,
 * with the exception of an optional ignore list of prop keys (strings).
 *
 * Can be provided as second argument to `React.memo` when using hooks, which is why a return of
 * `true` means don't update and `false` means do update.
 *
 * Note that ignoring state values isn't really all that useful with hooks, so it only accepts an
 * ignore list of props.
 *
 * Example:
 *
 *   export default memo ((...), shouldUpdate (['onClick', 'onMouseOver'))
 *
 * If `logger` is provided, logs the first prop which changed and wasn't ignored, or null.
 *
 * See the `memoWith...` functions for a different way to use this.
 */

export const shouldUpdateWith = invoke (() => {
  const getLogger = ifOk (
    (logger) => (changed) => logger (changed | ifOk (
      sprintf1 ('memo miss:\n the first changed (non-ignored) prop was %s'),
      () => 'memo hit:\n no changed (non-ignored) props',
    )),
    () => noop,
  )
  return (cmp, ignoreProps, logger=null) => cmp (ignoreProps)
    >> tap (getLogger (logger)) >> not
})

export const shouldUpdate = (ignoreProps, logger=null) => shouldUpdateWith (cmpEq, ignoreProps, logger)

/* Examples:
 *   export default LemmaList
 *     | memo
 *     | connect       (mapStateToProps, mapDispatchToProps)
 *   export default LemmaList
 *     | memoIgnore ([])
 *     | connect       (mapStateToProps, mapDispatchToProps)
 *   export default LemmaList
 *     | memoIgnore (['selectedLemmaIdx])
 *     | connect       (mapStateToProps, mapDispatchToProps)
 *   export default LemmaList
 *     | memoIgnoreTell (someLogFunction, ['selectedLemmaIdx'])
 *     | connect       (mapStateToProps, mapDispatchToProps)
 *
 * You can use `memoIgnoreTell` instead of `memoIgnore` and provide for example the component name
 * as `tag` to see which prop causes the memo to recalculate. Generally it's better to use
 * `whyRender`, because you can easily disable/enable it with a flag and because it gives more
 * useful information.
 */

export const memoWithIgnore = recurry (3) (
  ignore => cmp => component => memo (component, shouldUpdateWith (cmp, ignore)),
)

export const memoWithIgnoreTell = recurry (4) (
  logger => ignore => cmp => component => memo (component, shouldUpdateWith (cmp, ignore, logger)),
)

export const memoIgnoreTell = recurry (3) (
  logger => ignore => component => memoWithIgnoreTell (logger, ignore, cmpEq, component),
)

export const memoIgnore = recurry (2) (
  ignore => component => memoWithIgnore (ignore, cmpEq, component),
)

const getMemoLogger = (tellSpec, componentName) => {
  if (!tellSpec) return null
  if (tellSpec !== true && !tellSpec [componentName])
    return null
  return (msg) => [componentName, msg] | sprintfN ('[component %s] %s') | debug
}

export const withMemoTell = recurry (4) (
  (tellSpec) => (componentName) => (memoSpec) => (comp) => memoSpec | ifOk (
    (ignoreProps) => memoIgnoreTell (getMemoLogger (tellSpec, componentName), ignoreProps, comp),
    () => comp,
  ),
)

export const withMemo = withMemoTell (null)

export const withEffect = (effect) => (comp) => (props, context) => {
  effect (props, context)
  return comp (props)
}

/* Create a ref and immediately sets its `current` property.
 *
 * Examples:
 *
 *   const offset = useRefValue (10)
 *   const onClick = useRefValue ((event) = ...)
 */

export const useRefValue = v => lets (
  () => useRef (void 8),
  (ref) => ref.current = v,
)

export const useCallbackConst = x => useCallback (x, [])

export const useRefSet = (v) => lets (
  () => useRef (v),
  (ref) => (w) => ref.current = w,
  (ref, set) => (f) => ref.current | f | set,
  (ref, set, update) => [ref, set, update],
)

export const useCallbackRef = () => {
  const [mounted, setMounted] = useState (null)
  const callbackRef = useCallbackConst (whenOk (
    (node) => setMounted (true),
  ))
  return [mounted, callbackRef]
}

// ------ note that all the measurement functions do a state update, so don't forget to update any
// `whyRender` calls with 'measure'.

export const useMeasureWithCb = recurry (2) (
  (cb) => (f) => {
    const [measurement, setMeasurement] = useState (null)
    const callbackRef = useCallbackConst (whenOk (
      (node) => {
        const measure = () => lets (
          () => node.getBoundingClientRect (),
          (rect) => setMeasurement (rect | f),
        )
        cb (node, measure)
        measure ()
      }
    ))
    return [measurement, callbackRef]
  },
)

export const useMeasureWith = noop | useMeasureWithCb
export const useMeasure = () => id | useMeasureWith

export const useMeasureWithStore = recurry (2) (
  (store) => useMeasureWithCb (
    (node, ..._) => store.current = node,
  ),
)

export const useMeasureStore = (store) => useMeasureWithStore (store) (id)

const makeMapStateToProps = (table) => createStructuredSelector (
  table,
)

const makeMapDispatchToProps = (table) => (dispatch) => table | mapTuples (
  (k, v) => [k, v >> dispatch],
)

export const componentTell = recurry (3) (
  (tellSpec) => ([componentName, memoSpec=[]]) =>
    withDisplayName (componentName) >> withMemoTell (tellSpec, componentName, memoSpec),
)

export const component = null | componentTell

// --- memo must come before connect, or else memoisation won't work right -- probably `connect` is
// wrapping us in a legacy-style class or something like that.

export const containerTell = recurry (3) (
  (tellSpec) => ([componentName, dispatchTable, selectorTable, memoSpec=[]]) =>
    withDisplayName (componentName) >>
    withMemoTell (tellSpec, componentName, memoSpec) >>
    connect (
      selectorTable | makeMapStateToProps,
      dispatchTable | makeMapDispatchToProps,
    ),
)

export const container = null | containerTell

export const loadable = (importFunc, { fallback = null, } = { fallback: null, }) => lets (
  () => importFunc | lazy,
  (Component) => (props) => createElement (
    Suspense,
    { fallback, },
    createElement (
      Component,
      props,
    )
  ),
)
