import {
  pipe, compose, composeRight,
  id, noop, divideBy, always, map, addIndex, ok, whenOk, dot, dot1,
  ifPredicate, whenPredicate, eq, lt, gt, ifFalse, ifTrue, concat,
  condS, cond, otherwise, guard, sprintf1, lets, blush,
  prop, dot2, exception, raise, invoke, defaultTo, ne, ifOk,
  whenTrue, join, T, F, not, tap, list, path, prepend, whenNotOk,
  assoc, notOk, toThe, isObject, isArray, mergeToM, reduce,
  letS, assocM, concatTo, assocPathM, split, againstAll, find,
  bindProp, recurry, head, side,
  flip, concatM,
  mapTuples,
  fromPairs,
  deconstruct,
  allAgainst, anyAgainst,
  appendM, lte, ifYes,
  merge, values, reduceRightC,
  ampersandN,
  remapTuples,
} from 'stick-js/es'

import { Just, Nothing, } from './bilby.js'
import { ifArray, ifEmptyString, } from './predicate.js'

// @stick
export const composeManyRight = (first, ...args) => reduce (
  (f, acc) => f >> acc,
  first,
  args
)

// --- endpoints inclusive.
export const between = recurry (3) (m => n => x => x >= m && x <= n)

export const reverseM = dot ('reverse')
export const reverse = xs => xs.reduceRight ((acc, x) => (acc.push (x), acc), [])

export const iwarn  = (...args) => console.warn ('Internal warning:', ...args)
export const ierror = (...args) => console.error ('Internal error:', ...args)
export const info   = (...args) => console.info (...args)
export const warn   = (...args) => console.warn (...args)
export const error  = (...args) => console.error (...args)
export const debug  = (...args) => console.debug (...args)

export const mapX = map | addIndex
export const trim = dot ('trim')
export const length = prop ('length')
export const slice = dot2 ('slice')
export const slice1 = dot1 ('slice')

// --- @deprecated (moved to predicate.js)
export const isEmptyList = prop ('length') >> eq (0)
export const isNotEmptyList = isEmptyList >> not
export const isEmptyString = isEmptyList
export const isWhiteSpaceOrEmpty = trim >> isEmptyString
//

export const tellIf = ifFalse (
  _ => noop,
  _ => (...args) => console.log (...args),
)

export const resolveP = (...args) => {
  console.log ('alleycat-js/general/resolveP is deprecated, please use alleycat-js/async/resolveP')
  return Promise.resolve (...args)
}
export const rejectP  = (...args) => {
  console.log ('alleycat-js/general/rejectP is deprecated, please use alleycat-js/async/rejectP')
  return Promise.reject (...args)
}
export const allP     = (...args) => {
  console.log ('alleycat-js/general/allP is deprecated, please use alleycat-js/async/allP')
  return Promise.all (...args)
}
export const startP   = _ => {
  console.log ('alleycat-js/general/startP is deprecated, please use alleycat-js/async/startP')
  return null | resolveP
}

// --- Number -> String -> String
export const prettyBytes = invoke (() => {
  const row = fmt => (pred, n, suffix) =>
    1024 | toThe (n + 1) | pred | guard (
      divideBy (1024 | toThe (n)) >> sprintf1 (fmt) >> concat (' ' + suffix)
    )

  return numDecimals => lets (
    ()          => numDecimals | sprintf1 ('%%.%sf'),
    (fmt)       => fmt | row,
    (_, rowFmt) => condS ([
        rowFmt (lt, 0, 'b'),
        rowFmt (lt, 1, 'k'),
        rowFmt (lt, 2, 'M'),
        rowFmt (lt, 3, 'G'),
        rowFmt (_ => T, 3, 'G'),
    ]),
  )
})

export const isTrue = eq (true)
export const isFalse = eq (false)

// @test
export const allUniqueAndOkAnd = (pred) => (xs) => {
  const seen = new Map
  const notPred = pred >> not
  for (const x of xs) {
    if (x | notPred) return false
    if (x | notOk) return false
    if (seen.has (x)) return false
    seen.set (x, true)
  }
  return true
}

export const allUniqueAndOk = allUniqueAndOkAnd (T)

export const compareDates = (a, b) => lets (
  _ => a | Number,
  _ => b | Number,
  (an, bn) => an === bn ? 0 : an < bn ? -1 : 1
)

/*

test allUniqueAndOk

; [
    [void 8, void 8],
    [void 8, null],
    [void 8, 3],
    [1, 1],
    [1, 2],
    ['1', 1],
    [null, 1],
    [null, null],
    [null, void 8],
    [1, 1, 1, 1, 1],
    [1, 1, 1, 1, 2],
    [1, 2, 3, 4, 5],
]
*/

export const checkUploadFilesLength = (files, alertFunc = noop) => files.length | condS ([
  0 | eq | guard (_ => ierror ('empty file list')),
  1 | gt | guard (_ => 'Too many files'
    | tap (alertFunc)
    | F
  ),
  otherwise | guard (T),
])

export const checkUploadFilesSize = ({
  file,
  maxFileSize: max = 1024 * 1024,
  prettyBytesDecimalPlaces: places = 0,
  alertFunc = noop,
}) => file.size | condS ([
  max | gt | guard (_ => max
    | prettyBytes (places)
    | sprintf1 ('File too large! (max = %s)')
    | tap (alertFunc)
    | F
  ),
  otherwise | guard (T),
])

// export const nullToUndef = ifNull (noop) (id)

export const color = x => ['#', x] | join ('')

// --- android webview only shows the first arg by default; note that logging objects won't work
// without an inspect () call.
export const logAndroid = list >> join (',') >> console.log

export const logAndroidPerf = (...args) => logAndroid (...(args | prepend (performance.now ())))

export const log = console | bindProp ('log')
export const logWith = (header) => (...args) => log (... [header, ...args])
export const infoWith = (header) => (...args) => info (... [header, ...args])

export const divMod = m => n => lets (
  _ => n % m,
  (mod) => (n - mod) / m,
  (mod, div) => [div, mod],
)

export const andN = (...args) => {
  for (const i of args)
    if (!i) return false
  return true
}

export const defaultToV = recurry (2) (
  (v) => defaultTo (v | always),
)

export const mergeAll = xs => xs | reduce ((tgt, src) => mergeToM (tgt, src), {})

export const singletonArray = ifArray (id, x => [x])

/*
export const addCommas = invoke (() => {
  const ifLengthUnder4 = ifPredicate (length >> lt (4))
  const comma = ifLengthUnder4 (
    id,
    letS ([
      splitAt (3),
      (_, [b, c]) => [b, c | comma] | join (','),
    ]),
  )
  return String >> reverse >> comma >> reverse
})
*/

const { hasOwnProperty: hasOwn, } = {}

const _reduceObjDeep = (f) => (acc) => (path) => (o) => {
  let curAcc = acc
  for (const k in o) if (hasOwn.call (o, k)) {
    path.push (k)
    const v = o [k]
    // --- also arrays
    if (typeof v === 'object') curAcc = _reduceObjDeep (f) (acc) (path) (v)
    else curAcc = f (curAcc, [k, v, path])
    path.pop ()
  }
  return curAcc
}

// xxx stick
export const reduceObjDeep = (f) => (acc) => (o) => _reduceObjDeep (f) (acc) ([]) (o)

export const getQueryParams = () => document.location.search
  | defaultToV ('?')
  | slice1 (1)
  | ifEmptyString (
    () => ({}),
    split ('&') >> map (split ('=') >> deconstruct (
      ([k, v]) => [k, v | decodeURIComponent],
    )) >> fromPairs,
  )

export const getQueryParam = key => getQueryParams () | prop (key)

// --- be careful, toString is also a built-in function and forgetting to import it can lead to an
// annoying bug.
export const toString = dot ('toString')
export const toString1 = dot1 ('toString')

// --- functions which have been useful at some point or other.
const okAndNotFalse = againstAll ([ok, false | ne])
const ifOkAndNotFalse = okAndNotFalse | ifPredicate

; `

Usage:
  const threeAtATime = 3 | nAtATime
  ; [1, 2, 3, 4, 5, 6] | threeAtATime // => [[1, 2, 3], [4, 5, 6]]

Note: there are two versions now, the second one does not discard the chunk at the end.
`
const __nAtATime = (keepChunk) => (n) => (xs) => {
  const ret = []
  let i = -1, j = 0
  let cur = [], curlen = 0
  const len = xs | length
  for (let j = 0; j < len; j++) {
    const x = xs [j]
    curlen += 1
    cur.push (x)
    if ((curlen === n) || (keepChunk && j === len-1)) {
      ret.push (cur)
      cur = [], curlen = 0
    }
  }
  return ret
}

export const nAtATime = __nAtATime (false)
export const nAtATimePlusChunk = __nAtATime (true)

export const roundUp = letS ([
  Math.floor,
  (x, floor) => x === floor ? x : floor + 1,
])

export const roundDown = Math.floor

// --- babel bug prevents one-liner
const { round, } = Math
export { round, }

// @todo
// e.g. argTuple (0, 2) -> (x, _, y) -> [x, y]
// const argTuple

export const min = recurry (2) (a => b => Math.min (a, b))
export const max = recurry (2) (a => b => Math.max (a, b))

export const notBelow = max
export const notAbove = min

// --- @todo move to stick
// --- @todo check currying
// @check only necessary in order to ensure that the result has a well-defined arity, vs. rest args

// --- This is a version of `addIndex` for a functions with arity 2, e.g. `reduce`.
export const addIndex2 = (orig) => (f) => (x) => (coll) => {
    let idx = -1
    const g = (...args) => f (...args, ++idx)
    return orig (g) (x) (coll)
}

export const reduceX = reduce | addIndex2

; `
Replacement for URLSearchParams, which causes problems on IE, even with polyfill.

Usage:
  fetch ({
    ...
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    },
    body: { a, b, c, } | encodeFormData,
  })

`

export const encodeFormData = remapTuples (list >> map (encodeURIComponent) >> join ('=')) >> join ('&')

// --- @todo zip functions to stick

// --- note, not curried.
export const zipWithAll = (f, ...xss) => {
  const ret = []
  const n = Math.min (...xss.map (xs => xs.length))
  for (let i = 0; i < n; i++) ret.push (
    f (...xss.map (xs => xs [i])),
  )
  return ret
}

export const zipWithN = recurry (2) (
  f => xs => zipWithAll (f, ...xs),
)

export const zipWith3 = recurry (4) (
  (f) => (xs) => (ys) => (zs) => zipWithAll (f, xs, ys, zs),
)

export const zip = (...xss) => xss | zipWithN (list)

export const partition = recurry (2) (
  p => flip (reduce) (
    [[], []],
    ([left, right], x) => p (x)
      ? [left | appendM (x), right]
      : [left, right | appendM (x)],
  ),
)

// --- old-fashioned way to 'subclass' a class, in this case to make a custom throwable Error.
export function PromiseCancellation () {}
PromiseCancellation.prototype = Object.create (Error.prototype)

export const headMaybe = xs => xs.length === 0 ? Nothing : xs | head | Just

export const toHex = toString1 (16) >> concatTo ('0x')
export const curlyQuote = sprintf1 ('“%s”')

export const pathDot = split ('.') >> path

// --- @todo move to stick
; `immutable pop`
export const pop = (xs) => [...xs] | side ('pop')

export const setTimeoutOn = recurry (2) (
  f => ms => setTimeout (f, ms)
)

export const setTimeoutTo = setTimeoutOn | flip

export const setIntervalOn = recurry (2) (
  f => ms => setInterval (f, ms)
)

export const setIntervalTo = setIntervalOn | flip

export const eqZip = ys => xs => zipWithAll (eq) (xs, ys) | allAgainst (Boolean)

export const pluckN = recurry (2) ((ps) => (o) => {
  const ret = {}
  for (const p of ps) ret [p] = o [p]
  return ret
})

// @experimental same as prop then?
export const pluck = recurry (2) (p => o => pluckN ([p]) (o))

// @experimental only manually curried
export const pluckAll = (...xs) => o => pluckN (xs) (o)

export const repluckN = recurry (2) ((ps) => (o) => {
  const ret = []
  for (const p of ps) ret.push (o [p])
  return ret
})

export const repluck = recurry (2) (p => o => repluckN ([p]) (o))
// @experimental only manually curried
export const repluckAll = (...xs) => o => repluckN (xs) (o)

export const intersperse = (f) => (xs) => {
  const l = xs.length
  if (l === 0 || l === 1) return xs
  const [y, ...ys] = xs
  return ys | flip (reduce) (
    [y],
    (acc, x) => acc | concatM ([f (), x]),
  )
}

export const addIndexF = (idxF) => (orig) => (f) => {
    let idx = -1
    const g = (...args) => f (...args, ++idx | idxF)
    return orig (g)
}

/*
const odd = x => x % 2 === 1
; [1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1] | partition (T) | console.log
; [1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1] | partition (F) | console.log
; [1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1] | partition (odd) | console.log
*/

export const truncate = recurry (2) (
  l => letS ([
    length >> lte (l) >> ifYes (
      () => id,
      () => slice (0, l) >> concat (' …'),
    ),
    (s, f) => s | f,
  ]),
)

// --- given an object and a list of keys, remove those keys and return a list of the remaining
// values.
// --- the order of the output list is the order returned by `values`.

export const valuesWithMask = recurry (2) (
  ignore => o => lets (
    () => ignore | reduceRightC (x => assocM (x, null), {}),
    (m) => o | merge (m) | values,
  ),
)

// --- like a 'reverse' map, hence the same: given an array of functions and a value, apply each
// function to the value.
// --- see stick-js for caveat about the symbol `ampersandN`
// 3 | pam ([inc, double, odd]) // => [4, 6, true]

export const pam = ampersandN

export const conformsTo = (shape) => (o) => {
  for (const k in shape) {
    const p = shape [k]
    if (!p (o [k])) return false
  }
  return true
}

/*
export const toSet = xs => new Set (xs)
export const setAddM = x => s => (s.add (x), s)
export const setAddToM = s => x => (s.add (x), s)
export const setHas = bindProp ('has')
export const ifSetHas = setHas >> ifPredicate
*/
