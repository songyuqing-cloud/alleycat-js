import {
  pipe, compose, composeRight,
  ok, whenOk, dot, dot1, tap,
  map,
  ifPredicate, whenPredicate,
  eq, lt, gt,
  ifFalse, ifTrue, noop,
  condS, cond, otherwise, guard,
  lets, blush,
  dot2, exception, raise, invoke, defaultTo, gte,
  againstAny,
  always,
  letS, prop, ifYes,
  mergeM,
  id, againstBoth,
  ifAlways,
  recurry, ifOk,
} from 'stick-js/es'

import { letsP, } from './lets-promise.js'

import 'whatwg-fetch'

import { then, recover, promiseToEither, thenTap, resolveP, rejectP, } from './async.js'
import { Left, Right, Just, Nothing, toMaybe, cata, fold, } from './bilby.js'
import { logWith, between, } from './general.js'
import {
  ResultOk, ResultUserError, ResultNonUserError,
  resultFoldMap, resultFold,
} from './internals/types.js'

export { resultFoldMap, resultFold, }

// --- JSON parsing is skipped for the following status codes.
const noBodyCodes = [204, 205]
const noBodyCodesPred = againstAny (noBodyCodes | map (eq))

// --- note: res.json () returns a promise.
const parseJSON = letS ([
  prop ('status') >> noBodyCodesPred,
  (res, noRes) => noRes ? null : res.json (),
])

/*
const responseErrorProto = {
  response: void 8,
  err: void 8,
  toString () {
    return 'Response failed status check: ' + this.err
  },
}

// --- expression syntax is not possible for some reason ('object not extensible')
const responseError = response => {
  return Object.create (responseErrorProto) | mergeM ({
    response,
    err: response.statusText,
  })
}

const checkStatus = pred => res => lets (
  () => res.status,
  (code) => code | pred,
  (code, ok) => ok | tap (logWith ('ok')) | ifTrue (
    () => res,
    () => res.json () | then (body => [code, body] | rejectP),
  ),
)

const checkStatus200 = checkStatus (againstBoth (200 | gte, 300 | lt))
*/

export const defaultOpts = {
  headers: {
    'Content-Type': 'application/json',
  },
  credentials: 'include',
}

; `

@todo update docs

request passes a url and optional options to whatwg-fetch.
It rejects if the status is not in the range [200, 300), and then it parses the results as JSON.
Finally it converts the result to an Either.

Usage in saga:

  const results = yield call (request, url, options)
  yield results | fold (
    left => ...,
    right => ...,
  )

Usage in normal functions:

  request (url, options)
  | then (fold (
    left => ...,
    right => ...,
  ))
`

/*
 * The response is assumed to contain a JSON payload, regardless of status code.
 *
 * If the request fails or the JSON doesn't parse -> reject and then resolve with ResultNonUserError.
 * If the status is ok, resolve with ResultOk.
 * If the status is 4xx and there's a umsg, resolve with ResultUserError.
 * If the status is 4xx and there's no umsg, resolve with ResultNonUserError.
 * If the status is 5xx, resolve with ResultNonUserError.
 * Otherwise, resolve with ResultNonUserError.
 */

export const requestJSON = recurry (2) (
  url => options => letsP (
    () => fetch (url, options),
    (res) => res | parseJSON,
    (res, body) => [res.status, body.umsg, body.imsg],
    (res, body, [code, umsg, imsg]) => code | condS ([
      between (200, 299) | guard (() => body | ResultOk),
      between (400, 499) | guard (() => umsg | ifOk (
        ResultUserError,
        () => ResultNonUserError (imsg | toMaybe),
      )),
      between (500, 599) | guard (() =>
        ResultNonUserError (imsg | toMaybe),
      ),
      otherwise          | guard (() => imsg | rejectP),
    ]),
  )
  | recover ((err) => err | Just | ResultNonUserError)
)
