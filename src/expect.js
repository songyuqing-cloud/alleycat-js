// --- expect is a global symbol (provided by jest) and eslint knows about it.

import {
  pipe, compose, composeRight,
  recurry,
  dot1, dot, prop, path,
  passTo, getType,
  invoke, tap,
} from 'stick-js/es'

import { then, recover, } from './async.js'

// --- to-be is reference equality; to-equal matches similar objects.

export const toEqual = 'toEqual' | dot1
export const toBe = 'toBe' | dot1
export const toThrow = 'toThrow' | dot
export const toThrowA = 'toThrow' | dot1
export const not = 'not' | prop
export const toBeInstanceOf = 'toBeInstanceOf' | dot1
export const toBeTruthy = 'toBeTruthy' | dot
export const toBeFalsy = 'toBeFalsy' | dot
export const rejects = 'rejects' | prop
export const resolves = 'resolves' | prop

// --- anything is a function.
export const { anything, } = expect

export const expectToEqual = recurry (2) (
  expected => expect >> toEqual (expected),
)

export const expectToBe = recurry (2) (
  expected => expect >> toBe (expected),
)

export const expectNotToEqual = recurry (2) (
  expected => expect >> not >> toEqual (expected),
)

export const expectNotToBe = recurry (2) (
  expected => expect >> not >> toBe (expected),
)

export const expectOk = expectToEqual (anything ())
export const expectNotOk = expectNotToEqual (anything ())
export const expectNil = expectNotOk

export const expectToMatchRegex = recurry (2) (
  regex => expectToEqual (expect.stringMatching (regex)),
)

export const expectNotToMatchRegex = recurry (2) (
  regex => expectNotToEqual (expect.stringMatching (regex)),
)

export const expectToBeInstanceOf = recurry (2) (
  expected => expect >> toBeInstanceOf (expected),
)

export const expectNotToBeInstanceOf = recurry (2) (
  expected => expect >> not >> toBeInstanceOf (expected),
)

// --- checks constructor.name.
export const expectConstructorNameToBe = recurry (2) (
  expected => path (['constructor', 'name']) >> expectToBe (expected),
)

// --- checks constructor.name.
export const expectConstructorNameNotToBe = recurry (2) (
  expected => path (['constructor', 'name']) >> expectNotToBe (expected),
)

export const expectToBeTruthy = expect >> toBeTruthy
export const expectToBeFalsy = expect >> toBeFalsy

// --- tests on truthy.
export const expectPredicate = recurry (2) (
  p => passTo (p) >> expectToBeTruthy,
)

// --- tests on falsy.
export const expectNotPredicate = recurry (2) (
  p => passTo (p) >> expectToBeFalsy,
)

const typeOf = x => typeof x

// ------ using JS 'typeof' operator.

export const expectToHaveTypeof = recurry (2) (
  expected => typeOf >> expectToBe (expected),
)

export const expectNotToHaveTypeof = recurry (2) (
  expected => typeOf >> expectNotToBe (expected),
)

// ------ using stick-js 'getType' function, which works by calling Object.prototype.toString and
// parsing the result.

export const expectToHavePrimitiveType = recurry (2) (
  expected => getType >> expectToBe (expected),
)

export const expectNotToHavePrimitiveType = recurry (2) (
  expected => getType >> expectNotToBe (expected),
)

// ------ true if expected is a subset of received.

export const expectToContainObject = recurry (2) (
  expected => expectToEqual (expect.objectContaining (expected)),
)

export const expectNotToContainObject = recurry (2) (
  expected => expectNotToEqual (expect.objectContaining (expected)),
)

// ------ these return a promise: make sure your test returns a promise or uses `async`/`await`.

// --- expectToRejectWith is generally easier to use.
export const expectToRejectWithF = recurry (2) (
  catcher => invoke >> then (
    () => false | expectToBe (true),
  ) >> recover (catcher)
)

// --- there is no generic 'expectToReject' function because it's too generic (runtime errors might
// pass for example).
export const expectToRejectWith = recurry (2) (
  rejectValue => invoke >> expect >> rejects >> toEqual (rejectValue),
)

/* old implementation, possibly instructive.
export const expectNotToReject = invoke
  >> then (
    () => false | expectToBe (false),
  ) >> recover (
    () => false | expectToBe (true),
  )
*/

export const expectToResolve = invoke >> expect >> resolves >> toEqual (anything ())
export const expectToResolveWith = recurry (2) (
  resolveValue => invoke >> expect >> resolves >> toEqual (resolveValue),
)

export const expectToThrow = expect >> toThrow
export const expectToThrowA = recurry (2) (
  errorType => expect >> toThrowA (errorType),
)
export const expectNotToThrow = expect >> not >> toThrow
