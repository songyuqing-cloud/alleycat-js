// --- predicate has no alleycat-js dependencies.


import {
  pipe, composeRight, compose,
  prop, againstAll,
  ifPredicate, eq, gt,
  find,
  getType,
  isType,
  isObject, isArray,
  ifTrue, always, ifOk, ifFalse, ifNotOk,
  noop,
  whenPredicate, not,
  whenTrue, whenFalse,
  dot, ok, nil,
  recurry,
  ne,
  ifPredicateV, whenPredicateV,
  allAgainst,
  keys, list, anyAgainst,
} from 'stick-js/es'

const length = prop ('length')

// --- @todo
const isLeft = noop

export const ifLongerThan = n => ifPredicate (length >> gt (n))

export const isException = isType ('Error')
export const ifException = ifPredicate (isException)
export const ifNegativeOne = ifPredicate (-1 | eq)

export const ifSingletonLeft = ifPredicate (
  againstAll ([
    isArray,
    length >> eq (1),
    prop (0) >> isLeft
  ])
)

export const ifArray = isArray | ifPredicate
export const ifObject = isObject | ifPredicate
export const isNull = null | eq
export const ifNull = ifPredicate (isNull)
export const isTrue = true | eq
export const isFalse = false | eq

// --- @todo move to stick?
export const complement = f => (...args) => f (...args) | not
export const whenNotPredicate = complement >> whenPredicate

export const ifTrueV = isTrue | ifPredicateV
export const ifFalseV = isFalse | ifPredicateV
export const ifOkV = ok | ifPredicateV
export const ifNotOkV = nil | ifPredicateV
export const whenTrueV = isTrue | whenPredicateV
export const whenFalseV = isFalse | whenPredicateV

// --- assumes that it *is* a list, and checks the length.
export const isEmptyList = prop ('length') >> eq (0)
export const isNotEmptyList = isEmptyList >> not

// --- assumes that it *is* an object, and checks the keys.
export const isEmptyObject = keys >> isEmptyList
export const isNotEmptyObject = isEmptyObject >> not

// --- assumes that it *is* a string, and checks it.
export const isEmptyString = isEmptyList
export const isNotEmptyString = isEmptyString >> not
export const isWhiteSpaceOrEmpty = dot ('trim') >> isEmptyString

export const ifEmptyList = isEmptyList | ifPredicate
export const ifEmptyString = isEmptyString | ifPredicate

// @todo eq >> ifPredicate
export const ifEquals = recurry (3) (
  x => ifPredicate (x | eq),
)

// @todo recurry
export const whenEquals = eq >> whenPredicate
export const ifEqualsV = x => ifPredicateV (x | eq)
export const whenEqualsV = eq >> whenPredicateV

export const ifNotEquals = recurry (3) (
  x => ifPredicate (x | ne),
)

// @todo recurry
export const whenNotEquals = ne >> whenPredicate
export const ifNotEqualsV = x => ifPredicateV (x | ne)
export const whenNotEqualsV = ne >> whenPredicateV

// @stick
export const ifPredicateResult = recurry (4) (
  (f) => (yes) => (no) => (x) => {
    const p = f (x)
    return p ? yes (x, p) : no (x)
  }
)
export const whenPredicateResult = recurry (3) (
  f => yes => ifPredicateResult (f) (yes) (noop)
)

export const notTrue = true | ne
export const whenNotTrue = notTrue | whenPredicate
export const whenNotTrueV = x => whenNotTrue (_ => x)
export const ifNotTrue = notTrue | ifPredicate

export const whenNotEmptyString = isNotEmptyString | whenPredicate
export const whenNotEmptyList = isNotEmptyList | whenPredicate

export const okAndNotFalse = againstAll ([ok, false | ne])
export const ifOkAndNotFalse = okAndNotFalse | ifPredicate

export const ifType = isType >> ifPredicate

export const bothOk = (x, y) => [x, y] | allAgainst (ok)
export const allOk = allAgainst (ok)
export const whenAllOk = allOk | whenPredicate
export const ifAllOk = allOk | ifPredicate

/* `all` and `any` are similar to `lets`.
 *
 * `all` is like `lets` with an abort: if any of the entries returns falsey, it stops.
 * `any` also stops on the first truthy values it encounters.
 *
 * The `all` variants return the last evaluated value (or `false`) instead of returning `true`.
 * The `any` variants return the value (or `false`) instead of returning `true`.
 */

export const allV = list >> allAgainst (Boolean)
export const anyV = list >> anyAgainst (Boolean)

export const all = (...fs) => {
  const acc = []
  let last
  for (const f of fs) {
    const ret = f (...acc)
    if (!ret) return false
    acc.push (ret)
    last = ret
  }
  return last
}

// --- @todo return value, not true
export const any = (...fs) => {
  const acc = []
  for (const f of fs) {
    const ret = f (...acc)
    if (ret) return true
    acc.push (ret)
  }
  return false
}

export const allN = xs => all (...xs)
export const ifAllN = recurry (3) (
  (ja) => (nee) => allN >> ifTrue (ja) (nee),
)
export const whenAllN = recurry (2) (
  (ja) => allN >> whenTrue (ja),
)

export const allNV = xs => allV (...xs)
export const ifAllNV = recurry (3) (
  (ja) => (nee) => allNV >> ifTrue (ja) (nee),
)
export const whenAllNV = recurry (2) (
  (ja) => allNV >> whenTrue (ja),
)

// @experimental, can't curry.
export const ifAll = (ja) => (nee) => (...xs) => all (...xs) | ifTrue (ja) (nee)
export const whenAll = (ja) => (...xs) => all (...xs) | whenTrue (ja)
