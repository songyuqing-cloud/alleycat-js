import {
  pipe, compose, composeRight,
  ok, ifOk, ifPredicate, whenOk, whenPredicate,
  id, tap, recurry,
  map, filter, reject, reduce, flip, flip3,
  join, split, last, head, tail,
  dot, dot1, dot2, side, side1, side2,
  cond, condS, guard, guardV, otherwise,
  sprintf1, sprintfN,
  noop, blush, always, T, F,
  prop, path, invoke, lt,
  lets, letS,
  die, eq, ne,
  factory, factoryProps,
} from 'stick-js/es'

const { log, } = console

import { all, allV, any, anyV, } from './predicate.js'

allV (3, 4, 5) | log
allV (3, null, 5) | log
anyV (3, null, 5) | log
anyV (void 8, null, false) | log

all (
  () => 3 + 3,
  (six) => six + 10,
) | log

all (
  () => 3 + 3,
  (six) => six - 6,
) | log

; [
  'UserAgent AppleWebKit/606',
  'UserAgent AppleWebKit/608',
  'UserAgent Firefox',
] | map ((x) => all (
  () => x.match (/AppleWebKit\/(\d+)/),
  prop (1) >> Number >> lt (607),
)| log)
