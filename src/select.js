import {
  pipe, compose, composeRight,
  recurry, path, sprintfN,
  prop, not,
} from 'stick-js/es'

// --- @peer
import reselect from 'reselect'
const { createSelector, } = reselect

import { info, defaultToV, } from './general.js'

export const selectTell = recurry (5) (
  (tellSpec) => (sliceName) => (selectorName) => (deps) => (f) => {
    let recomputations
    const selector = createSelector (
      ...deps,
      f,
    )
    const tell = (recomputations) => {
      if (!tellSpec) return
      if (tellSpec !== true && (tellSpec | path ([sliceName, selectorName])) | not) return
      info ([sliceName, selectorName] | sprintfN ('[select %s/%s] reselect'), recomputations)
    }
    return (state, _) => {
      const newRecomputations = selector.recomputations ()
      if (recomputations !== newRecomputations) tell (newRecomputations)
      recomputations = newRecomputations
      return selector (state)
    }
  }
)

export const initSelectorsTell = recurry (3) (
  (tellSpec) => (sliceName) => (initialState) => {
    // --- we throw the props away to greatly simplify memoisation.
    // if you really need to pass them to the selector, you can either have the selector return a
    // function which accepts the needed props as arguments, or use createSelector directly.
    const selectSlice = (store, _) => store | prop (sliceName) | defaultToV (initialState)

    const select = selectTell (tellSpec, sliceName)
    const selectTop = (selectorName, f) => select (selectorName, [selectSlice], f)
    const selectVal = (propName) => selectTop (propName, prop (propName))

    return {
      select,
      selectTop,
      selectVal,
    }
  }
)

export const initSelectors = null | initSelectorsTell
