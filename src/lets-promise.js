import {
  pipe, compose, composeRight,
  ok, ifOk, ifPredicate, whenOk, whenPredicate,
  id, tap, recurry, roll,
  map, filter, reject, reduce, flip, flip3,
  join, split, last, head, tail,
  dot, dot1, dot2, side, side1, side2,
  cond, condS, guard, guardV, otherwise,
  sprintf1, sprintfN,
  noop, blush, always, T, F,
  prop, path, has, hasIn,
  bindPropTo, bindProp, bindTo, bind,
  assoc, assocPath, assocM, assocPathM,
  concatTo, concat, appendTo, append,
  concatToM, concatM, appendToM, appendM,
  merge, mergeTo, mergeM, mergeToM,
  mergeIn, mergeInTo, mergeInM, mergeInToM,
  updateM, update, updatePathM, updatePath,
  lets, letS, compactOk, compact,
  die, raise, decorateException, exception,
  lt, gt, eq, ne, lte, gte,
  factory, factoryProps,
  passToN,
} from 'stick-js/es'

import { startP, resolveP, allP, then, recover, } from './async.js'

import TailList from './internals/tailList.js'

const composeManyRight = (first, ...args) => args.reduce (
  (acc, f) => (...args) => f (acc (...args)),
  first,
)

const composeManyRightN = passToN (composeManyRight)

const tapPSym = Symbol ('tapPSym')

export const tapP = then (() => tapPSym | resolveP)

export const letSP = fs => x => letsP (
  () => x,
  ...fs,
)

export const letsNP = fs => letsP (...fs)

export const letsP = (...fs) => {
  const values = TailList.create ({ n: 1, }).init ()
  const sequence = f => then ((x) => {
    if (x !== tapPSym) values.push (x)
    return f (...(values.get ()))
  })
  const sequenced = fs | map (sequence) | composeManyRightN
  return startP () | sequenced
}

// export const sequenceP = (ps) => ps | map (always) | passToN (letsP)

export const reduceC = recurry (3) ((f) => (acc) => (xs) => {
  let acco = acc
  for (let i = 0, l = xs.length; i < l; i += 1) {
    let g = f (acco) (xs [i])
    acco = g
  }
  return acco
})

export const reduceRightC = recurry (3) ((f) => (acc) => (xs) => {
  let acco = acc
  const l = xs.length
  if (l === 0) return acco
  for (let i = l - 1; i >= 0; i -= 1) {
    let g = f (xs [i]) (acco)
    acco = g
  }
  return acco
})
