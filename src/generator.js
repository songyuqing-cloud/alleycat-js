import {
  pipe, composeRight, compose,
  dot, prop,
} from 'stick-js/es'

export const next = dot ('next')
export const done = prop ('done')
export const value = prop ('value')
