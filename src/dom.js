import {
  pipe, composeRight, compose,
  each, path, prop,
  recurry, dot1, tap, noop,
  list, compact, join, side, dot,
  side2,
} from 'stick-js/es'

import scrollIntoViewMod from 'scroll-into-view-if-needed'

import { whenEquals, } from './predicate.js'

export const inputValue = path (['target', 'value'])

/*
 * Usage:
 *   ; ['css-selector', 'css-selector', ...] | hideAllChildrenOf (element)
 *
 *   Note: uses NodeList.forEach and hence needs a polyfill for some browsers.
 */

export const hideAllChildrenOf = el => each (
  sel => el.querySelectorAll (sel) | each (
    x => x.style.display = 'none'
  )
)

// @todo not onClick
// @todo pass event to f

/*
 * Usage:
 *   onClick={keyPressListen (onSubmit, 'Enter')}
 */

export const keyPressListen = recurry (3) (f => keyName => prop ('key') >> whenEquals (keyName) (f))
export const keyPressListenPreventDefault = f => keyName => (event) =>
  event | prop ('key') | whenEquals (keyName) (() => event
    | preventDefault
    | f
  )

/*
 * Usage:
 *   ref | eachSelector ('img') ((node) => ...)
 */

export const eachSelector = sel => f => dot1 ('querySelectorAll') (sel) >> each (f)

export const onEventNativeWithAfter = recurry (4) ((after) => (eventString) => (f) => (elem) => {
  const g = event => f (event, elem) | tap ((ret) => after (event, elem, ret))
  return elem | addEventListener (eventString, g)
})

export const onEventNative = noop | onEventNativeWithAfter

export const onEventNativeStopPropagation = onEventNativeWithAfter (
  (event, ..._) => event | stopPropagation
)

export const onClickNative = 'click' | onEventNative
export const onClickNativeStopPropagation = 'click' | onEventNativeStopPropagation

/*
 * Side-effect version of 'elem.classList.add (...)'
 * Note: might need polyfill for older browsers; IE10 & 11 should be ok.
 */

export const addClass = cls => tap (prop ('classList') >> dot1 ('add') (cls))

export const clss = list >> compact >> join (' ')

export const addEventListener = side2 ('addEventListener')
export const stopPropagation = side ('stopPropagation')
export const preventDefault = side ('preventDefault')

export const querySelector = dot1 ('querySelector')
export const scrollIntoView = recurry (2) (
  options => node => scrollIntoViewMod (node, options)
)
